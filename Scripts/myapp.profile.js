var profile = (function(){
    return {
        releaseDir: "release",
        action: "release",
        transform: 'writeAmd',
        defaultConfig: {
            hasCache:{
                "dojo-built": 1,
                "dojo-loader": 1,
                "dom": 1,
                "host-browser": 1,
                "config-selectorEngine": "lite"
            },
            async: 1
        },
        packages: [
            {
                name:'dojo',
                location: 'dojo'
            }, {
                name:'igit',
                location: 'igit'
            }
        ],

        layers: {
            "igit/start" : {
                //include: ['dojox/gfx/svg', 'dojox/gfx/shape', 'dojox/gfx/path']
                //exclude: ['esri/layers/ImageParameters']
                // customBase: true,
                // boot: true
            }
            /*,
            'dojo/dojo': {
                include: [],
                boot: true,
                customBase: true
            }*/
        }
    };
})();