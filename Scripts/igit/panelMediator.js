define([
    "dojo/_base/declare",
    "dojo/dom-class",
    'dijit/Tree',
    "dijit/tree/ObjectStoreModel"
],
    function(declare, domClass, Tree, ObjectStoreModel){
        function initFunc(){
            var myModel = new ObjectStoreModel({
                store: this.store,
                query: { id: 'rootNode' },
                labelType: "html",
                getLabel: function(item){
                    return '<b>' + item.id + '</b>';
                },
                mayHaveChildren: function(item){
                    return true;
                }
            });

            // Create the Tree.
            var tree = new Tree({
                model: myModel,
                showRoot: false
            });
            tree.placeAt(this.leftPanel);
            tree.startup();
        }
        function showLeftPanel(){
            domClass.add(this.leftPanel.domNode, 'active');
            this.appLayout.addChild(this.leftPanel);
        }

        function hideLeftPanel(){
            domClass.remove(this.leftPanel.domNode, 'active');
            this.appLayout.removeChild(this.leftPanel);
        }

        return declare(null, {
            constructor: function(params){
                this.appLayout = params.appLayout;
                this.leftPanel = params.leftPanel;
                this.store = params.store;
            },

            showLeftPanel: showLeftPanel,

            hideLeftPanel:hideLeftPanel,
            startup: initFunc
        })

});
