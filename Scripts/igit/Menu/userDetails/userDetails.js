define(["dojo/_base/declare",
    'dojo/_base/config',
    'dojo/string',
    'dojo/text!./template.html'
],
    function(decalre, config, string, template){
        function initFunc(){
            var userName = config.userName || 'testUser';
            template = string.substitute(template, { userName: userName });
            this.domNode.innerHTML = template;
        }

        return decalre(null, {
            constructor: function(params, domNode){
                this.domNode = domNode;
            },
            startup: initFunc
        });
    }
);