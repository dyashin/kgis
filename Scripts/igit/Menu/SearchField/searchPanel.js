define(['dojo/_base/declare', 'dojo/store/Memory', 'dijit/form/ComboBox'], function(declare, Memory, ComboBox ){
    function initFunction(){
        this.store = new Memory({
            data: [
                {name:"Alabama", id:"AL"},
                {name:"Alaska", id:"AK"},
                {name:"American Samoa", id:"AS"},
                {name:"Arizona", id:"AZ"},
                {name:"Arkansas", id:"AR"},
                {name:"Armed Forces Europe", id:"AE"},
                {name:"Armed Forces Pacific", id:"AP"},
                {name:"Armed Forces the Americas", id:"AA"},
                {name:"California", id:"CA"},
                {name:"Colorado", id:"CO"},
                {name:"Connecticut", id:"CT"},
                {name:"Delaware", id:"DE"}
            ]
        });

        this.comboBox = new ComboBox(
            {
                id: "stateSelect",
                name: "state",
                // value: "California",
                store: this.store,
                searchAttr: "name"
            }, /*'search-field'*/ this.domNode);
    }

    var a = {
        domNode: null,
        comboBox: null,
        options: null,
        store: null,
        constructor: function(params, domNode){
            this.domNode = domNode;
            this.options = params;
        },

        startup: initFunction
    }

    return declare(null, a);
});