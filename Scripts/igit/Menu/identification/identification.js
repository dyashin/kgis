define(["dojo/_base/declare", "dojo/Evented"], function(declare, Evented){

    function initFunc(){
        this.domNode = {}; // бла-бла создать кнопку - генерация вьюхи
        this.domNode.on('click', this.emit('click'));
    }

    var ans = {
        startup: initFunc,
        domNode: null
    };

    return declare([Evented], ans);

})