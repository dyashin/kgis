define(['dojo/_base/declare', 'dojo/on', 'dojo/dom-class'], function(declare, on, domClass){
    return declare(null, {
        _value: false,
        constructor: function(params, node){
            this.domNode = node;
            var params = params || {};
            this.onClick = params.onClick;

            // todo вообще-то плохо, надо обработчик выше вынести
            var self = this;
            on(this.domNode, 'click', function(){
                self._value = !self._value;
                if (self._value){
                    domClass.add(self.domNode, 'active');
                }else{
                    domClass.remove(self.domNode, 'active');
                }

                if (typeof self.onClick==="function"){
                    self.onClick(self._value);
                }
            });
        }
    });
});
