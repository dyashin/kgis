define([
    "dojo/_base/declare",
    "dojo/html",
    "dojo/query",
    "igit/Menu/toggleButton",
    "igit/Menu/identification/identification",
    "igit/Menu/searchField/searchPanel",
    "igit/Menu/userDetails/userDetails",
    'dojo/text!./menu.html'],
    function(declare, html, query, ToggleButton, Identification, SearchPanel, userDetails, template){
        function initFunc(){
            // html.set(this.domNode, template);
            this.domNode.innerHTML = template;

            // todo завернуть в цикл вместе с обработчиком
            var self = this;
            if (this.params.layers != false){
                this.layerButton = new ToggleButton({
                    onClick: function(active){
                        if (active){
                            self.panelMediator.showLeftPanel();
                            return;
                        }
                        self.panelMediator.hideLeftPanel();
                    }
                }, query('.menu-btn-layers', this.domNode)[0]);
            }

            if (this.params.identify !== false){
                this.identify = new ToggleButton(null, query('.menu-btn-identify', this.domNode)[0]);
            }

            if (this.params.zoom !== false){
                this.zoom = new ToggleButton(null, query('.menu-btn-search', this.domNode)[0]);
            }

            if (this.params.draw !== false){
                this.draw = new ToggleButton(null, query('.menu-btn-draw', this.domNode)[0]);
            }

            if (this.params.task !== false){
                this.task = new ToggleButton(null, query('.menu-btn-tasks', this.domNode)[0]);
            }

            if (this.params.round !== false){
                this.round = new ToggleButton(null, query('.menu-btn-round', this.domNode)[0]);
            }

            if (this.params.options !== false){
                this.options = new ToggleButton(null, query('.menu-btn-options', this.domNode)[0]);
            }



            // инициализация компонента поиска
            if (this.params.comboBox !== false){
                this.comboBox = new SearchPanel(null, query('.search-field', this.domNode)[0]);
                this.comboBox.startup();
            }
            if (this.params.userDetails !== false){
                this.userDetails = new userDetails(null, query('.user-details', this.domNode)[0]);
                this.userDetails.startup();
            }
        }

        var ans = {
            /**
             * @param domNode id или узел dom, в котором будет строиться меню
             * @param params search - true, indication-true
             */
            constructor: function(domNode, params){
                this.domNode = domNode;
                this.params = params || {};
                this.comboBox = null;
                this.userDetails = null;
                this.panelMediator = params.panelMediator;
            },

            identification: null,
            startup: initFunc
        };

        return declare(null, ans);
    }
)