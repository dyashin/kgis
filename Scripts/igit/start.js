/**
 * Контроллер приложения. Свзяка хранилища и карты.
 */
define([
    "dojo/_base/declare"
    , "dojo/query"
    , "dojo/_base/config"
    , "dijit/layout/LayoutContainer"
    , "dijit/layout/ContentPane"
    , "igit/map/map"
    , 'igit/Menu/menu'
    , 'igit/panelMediator'
    , "igit/store/layersStore"],
    function(declare, query, config, LayoutContainer, ContentPane, Map, Menu, PanelMediator, layersStore){
        var mapDiv,
            map,
            layerStore,
            resultInfoLayers,
            resultBaseLayers;

        /**
         * отвечающает за изменения настроек информационных слоев
         * @param layerWrapper
         * @param removedFrom
         * @param insertedInto
         */
        function observeInfoLayersChanges(layerWrapper, removedFrom, insertedInto){
            if (removedFrom === -1 && insertedInto != -1){
                // индекс не выставляется в связи с синхронностью производства слоёв.
                map.addLayer(layerWrapper.object);
            }
        }

        /**
         * Функция, отвечающая за смену подложки.
         * @param layerWrapper
         * @param removedFrom
         * @param insertedInto
         */
        function observeBaseLayersChanges(layerWrapper, removedFrom, insertedInto){
            if (removedFrom != -1 && insertedInto == -1){
                map.removeLayer(layerWrapper.object);
            }

            if (removedFrom == -1 && insertedInto != -1){
                map.addLayer(layerWrapper.object, 0);
            }
        }

        /**
         * Создание разметки
         */
        function initLayout(){
            var appLayoutDiv = query('.appLayout')[0];
            var appLayout = new LayoutContainer({
                design: "headline"
            }, appLayoutDiv);

            var headerMenuDiv = query('.header-menu', appLayoutDiv)[0];
            var headerMenu = new ContentPane({
                region: 'top'
            }, headerMenuDiv);
            appLayout.addChild(headerMenu);

            mapDiv = query('.center-map', appLayoutDiv)[0];
            var mapContainer = new ContentPane({
                region: 'center'
            }, mapDiv);

            appLayout.addChild(mapContainer);

            var leftDiv = query('.left-panel', appLayoutDiv)[0];
            var leftContainer = new ContentPane({
                region: 'left'
            }, leftDiv);

            appLayout.startup();

            var panelMediator = new PanelMediator({
                appLayout: appLayout,
                leftPanel: leftContainer
            });

            return panelMediator;
        }

        function mapIdentification(e, map){
            // это - твоя песочница. Если будет много, тогда выноситсья в отдельный файл.
        }

        function initFunction() {
            var panelMediator = initLayout();
            map = (new Map()).init(mapDiv);
            layerStore = new layersStore();
            var menu = new Menu(query('.header-menu')[0], {
                panelMediator: panelMediator
            });

            menu.startup();

            /*
            menu.identification.on("click", function(e){
                mapIdentification(e, map);
            });

*/            // Наблюдение за изменениями информационных уровней
            resultInfoLayers = layerStore.layers.query(function(item){return item.type == "layer"});
            resultInfoLayers.observe(observeInfoLayersChanges, true);

            // Смена базовой карта
            resultBaseLayers = layerStore.layers.query(function(item){return item.type == "baseMap" && item.active == true; });
            resultBaseLayers.observe(observeBaseLayersChanges, true);

            layerStore.startup(config.coreConfig.mapConfiguration);

            panelMediator.store = layerStore.layers;
            panelMediator.startup();
        }

        return declare(null, {
            init: initFunction
        });
});