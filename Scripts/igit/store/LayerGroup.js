﻿/**
 * Класс, представляющий собой группу слоев.
 */
define(['dojo/_base/declare', 'dojo/_base/array'], function (declare, array) {
    return declare(null, {
        /**
         * отображаемое имя в списке.
         */
        name: null,

        /**
         * определяет видимость всех слоев, содержащихся в группе.
         * 0 - невидимые,
         * 1 - видимые,
         * 2 - определяется настройкой слоя.
         */
        visible: false,

        /**
         * список слоев группы.
         */
        layers: null,

        /**
         * Конструктор класс
         * @param layerGroup - объект инициализации layerGroup-a. Берется из базового конфига.
         */
        constructor: function(layerGroup){
            if (layerGroup == null){
                return;
            }

            this.name = layerGroup.Id || layerGroup.id;
            this.id = this.name;
            this.visible = layerGroup.Visible || layerGroup.visible;
            this.opacity = 1;
            this.layers = [];
        },

        show: function(){
            array.forEach(this.layers, function(a){
                a.show();
            });

            this.visible = true;
        },

        hide: function(){
            array.forEach(this.layers, function(a){
                a.hide();
            });

            this.visible = false;
        },

        /**
         * Устанавливает значение прозрачности для всех слоев группы
         * @param value
         */
        setOpacity: function(value){
            array.forEach(this.layers, function(a){
                a.setOpacity(value);
            });

            this.opacity=value;
        },

        setVisibility: function(value){
            array.forEach(this.layers, function(a){
                a.setVisibility(value);
            });

            this.visible = value;
        }
    })
});