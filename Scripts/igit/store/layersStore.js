/**
 * Хранилище конфигураций слоев. Отвечает за соответствие слоя группе и порядок индекса. Сначала идут уровни в группах.
 * Структура: { customId, type, object, active }, где - customId - уникальный номер, type - тип объекта (слой, базовый слой или группа), object - сам объект,
 * active - идентификатор активности базовой карты
 * Для вложенных элементов - parentId.
 */
define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/Deferred',
    'dojo/_base/array',
    'dojo/store/Memory',
    'dojo/store/Observable',
    'igit/store/LayerGroup',
    'igit/esriLoader!esri/layers/ImageParameters'],
    function(declare, lang, Deferred, array, Memory, Observable, LayerGroup, ImageParameters){
        //todo Слегка изменить логику - id сделать уникальным(соответственно запретить создавать с таким именем),
        // а customId - параметром индекса для слоев.

        /**
         * Уникальное статическое число
         * @type {number}
         */
        var uniqueNumber = 0;

        /**
         * Создает слой по его описанию и добавляет в предоставленную группу слоев.
         * @param layerConfig - настройки слоя (layersGroup - add в конфиге)
         * @param groupLayerDesc - сама группа слоев.
         * @param layerConstructor - конструктор слоя.
         * @returns {} - слой типа layerConstructor
         * @private
         */
        function createLayer (layerConfig, groupLayerDesc, layerConstructor) {
            if (layerConfig.imageParameters) {
                var imageParams = new ImageParameters();
                lang.mixin(imageParams, layerConfig.imageParameters);
                layerConfig.imageParameters = imageParams;
            }

            if (groupLayerDesc){
                layerConfig.visible = groupLayerDesc.visible;
            }

            // Конструкторы OpenStreetMap и групп не имею url
            if (/(OpenStreetMapLayer|LayerGroup)/.test(layerConfig.type)){
                return new layerConstructor(layerConfig);
            }

            return new layerConstructor(layerConfig.url, layerConfig);
        }

        /**
         * возвращает уникальное число путем инкремента
         * @private
         */
        function _getCustomLayerId(){
            return uniqueNumber++;
        }

        /**
         * Получает объект, маппинг слоев и конструкторов
         * @param mapConfig
         * @return {{ requestParams: Array, constructorMap: {}, layerConfigs: Array }}
         * , где constructorMap - хэш индекса по названию конструктора
         * , requestParam - массив названий этих же конструкторов для предложения request
         * , layersConfig - массив конфигураций уровней
         */
        function getLayerConstructors(mapConfig){
            var ans,
                constructorMap,
                constructorName,
                layersArray,
                constructorCounter = 0,
                concatedArray;

            // Если в конфиге отсутстувует конфигурация дерева слоев и список baseMap-ов для выбора - выход.
            if (typeof mapConfig.layersTree === "undefined" && typeof mapConfig.maps === "undefined" ){
                return null;
            }

            constructorMap = {};
            layersArray = [];
            // соединение 2-х массивов, с одновременным проставлением типа элемента (слой, группа или базовый слой).
            concatedArray = array.map(mapConfig.layersTree, function(a){
                if (/LayerGroup/.test(a.type)){
                    a.treeType = "group";
                } else{
                    a.treeType = "layer";
                }

                return a;
            }).concat(
                    array.map(mapConfig.maps, function(a){ a.treeType = "baseMap"; return a; })
                );

            array.forEach(concatedArray, function(layerConfig){
                // todo засунуть сюда группы
                // если элемент группа - пропускаем его (основываясь на том, что у элемента нет типа)
                if (typeof layerConfig.type !== 'string'){
                    return;
                }

                // нужен так как после изменения списка слоев пользователем могут появиться идентичные слои.
                layerConfig.customId= _getCustomLayerId();
                layersArray.push(layerConfig);

                // если такой тип уже зарегистрирован
                if (typeof constructorMap[layerConfig.type] !== "undefined"){
                    return;
                }

                constructorMap[layerConfig.type] = constructorCounter ++;
            });

            ans = { requestParam: [], constructorMap: constructorMap, layersConfig: layersArray };

            for (constructorName in constructorMap){
                if (constructorMap.hasOwnProperty(constructorName)){
                    ans.requestParam.push(constructorName);
                }
            }

            return ans;
        }


        function initFunction(mapConfig){

            var totalLayerConstructors,
                self = this,
                objectToSave,
            // todo сейчас для залипухи - первый базовый слой становиться выбранным по умолчанию. В дальнейшем перенести в конфиг.
                activeBaseMapIsSet = false;
            totalLayerConstructors = getLayerConstructors(mapConfig);
            var rootId = _getCustomLayerId();
            var rootNode = { customId: rootId, id: "rootNode" }
            self.layers.put(rootNode);

            require(totalLayerConstructors.requestParam, function(){
                var i,
                    layer,
                    layerConfig;
                // добавление информационных уровней
                for (i = 0; i < totalLayerConstructors.layersConfig.length; i++ ){
                    layerConfig = totalLayerConstructors.layersConfig[i];
                    layer = createLayer(layerConfig, null, arguments[totalLayerConstructors.constructorMap[layerConfig.type]]);
                    objectToSave = {
                        type: layerConfig.treeType,
                        customId: layerConfig.customId,
                        object: layer,
                        active: false,
                        id: layer.id
                    };

                    //чтобы не отображать в дереве базовые слои
                    if (layerConfig.treeType !== "baseMap"){
                        objectToSave.parent = layerConfig.parent || "rootNode"
                    }

                    if (layerConfig.treeType === "baseMap" && !activeBaseMapIsSet){
                        objectToSave.active = true;
                        activeBaseMapIsSet = true;
                    }

                    self.layers.put(objectToSave);
                }
            });
        }

        var a = {
            layers: null,
            // layersGroups: null,
            startup: initFunction,
            constructor: function(){
                this.layers = new Observable(new Memory({
                    idProperty: "id",
                    getChildren: function(object){
                        return this.query({parent: object.id });
                    }
                }));
            }
        };

        return declare(null, a);
    }
);