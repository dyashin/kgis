define([
    "dojo/_base/declare",
    'dojo/_base/lang',
    "dojo/_base/config",
    "esri/map",
    "esri/config",
    "esri/tasks/GeometryService",
    "esri/geometry/Extent"
]
    , function(
        declare,
        lang,
        dojoConfig,
        Map,
        esriConfig,
        GeometryService,
        Extent
        )
    {
        var a = {
            init: function(divid){
                var mapOptions,
                    currentOptions,
                    proxyUrl;
                if (dojoConfig.coreConfig.mapConfiguration.geometry){
                    esriConfig.defaults.geometryService = new GeometryService(dojoConfig.coreConfig.mapConfiguration.geometry);
                }

                // var proxyUrl = BASEURL + 'proxy/proxy.ashx'
                proxyUrl = dojoConfig.coreConfig.baseUrl + 'proxy/proxy.ashx';

                esriConfig.defaults.io.proxyUrl = proxyUrl;

                if (dojoConfig.coreConfig.mapConfiguration.extent){
                    dojoConfig.coreConfig.mapConfiguration.extent = new Extent(dojoConfig.coreConfig.mapConfiguration.extent);
                }

                currentOptions = {
                    sliderOrientation: "vertical",
                    force3DTransforms: true,
                    navigationMode: 'css-transforms'
                }

                /// new Map(divid, mapOptions);

                mapOptions = lang.mixin(dojoConfig.coreConfig.mapConfiguration, currentOptions );

                // this.inherited(arguments, [divid, mapOptions]);
                var ans = new Map(divid, mapOptions);
                return ans;
            }
        };

        return declare(null, a);
});
